package com.voterapp.main;

import java.util.Scanner;

import com.voterapp.exception.InvalidVoterException;
import com.voterapp.service.ElectionBooth;
import com.voterapp.service.ElectionBoothImpl;

public class Voter {

public Voter() {
}

public static void main(String[] args) throws InvalidVoterException {

ElectionBooth electionBoothImpl = new ElectionBoothImpl();
Scanner sc = new Scanner(System.in);
System.out.println("Enter Age:");
int age = sc.nextInt();
System.out.println("Enter Locality:");
String locality = sc.next();
System.out.println("Enter voterID:");
int vid = sc.nextInt();
try {
System.out.println(electionBoothImpl.checkEligibility(age, locality, vid));
} catch (InvalidVoterException e1) {
System.out.println(e1.getMessage());
}finally {
sc.close();
}


}
}

