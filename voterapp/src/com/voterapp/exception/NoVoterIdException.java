package com.voterapp.exception;

public class NoVoterIdException extends InvalidVoterException {

	public NoVoterIdException() {
		super();
	}

	public NoVoterIdException(String arg0) {
		super(arg0);
	}

}
