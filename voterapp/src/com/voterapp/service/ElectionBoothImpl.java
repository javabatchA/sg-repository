package com.voterapp.service;

import com.voterapp.exception.InvalidVoterException;
import com.voterapp.exception.LocalityNotFoundException;
import com.voterapp.exception.InvalidVoterException;
import com.voterapp.exception.NoVoterIdException;
import com.voterapp.exception.UnderAgeException;

public class ElectionBoothImpl implements ElectionBooth {

public String[] localities= {"Namkum","bahubajar","bundu","kokar"};
public ElectionBoothImpl() {
}

@Override
public boolean checkEligibility(int age, String locality, int vid) throws InvalidVoterException {

try {
checkAge(age);
checkLocality(locality);
checkVoterId(vid);
}catch (InvalidVoterException e) {
throw new InvalidVoterException("Invalid voter details");
}
return false;
}

private boolean checkAge(int age) throws UnderAgeException{
if(age < 18) throw new UnderAgeException("Age MUST be above 18");
return true;
}
private boolean checkLocality(String lo) throws LocalityNotFoundException {
for (String locality:localities) {
if(lo.equals(locality) )
return true;
}
throw new LocalityNotFoundException("Locality does not comes under WhiteField");
}
private boolean checkVoterId(int vid) throws NoVoterIdException {
if(vid < 1000 || vid > 9999) throw new NoVoterIdException("Voter ID Must be between 1000 and 9999");
return true;
}

}