package com.voterapp.service;

import com.voterapp.exception.InvalidVoterException;
import com.voterapp.exception.LocalityNotFoundException;
import com.voterapp.exception.NoVoterIdException;
import com.voterapp.exception.UnderAgeException;

public class ElectionBoothImpl implements ElectionBooth {

	private String[] localities = { "new york" };

	@Override
	public boolean checkEligibility(int age, String locality, int vid) throws InvalidVoterException {

		try {
			if (!checkAge(age))
				return false;
			if (!checkVoterId(vid))
				return false;
			if (!checkLocality(locality))
				return false;

		} catch (InvalidVoterException e) {
			System.out.println(e.getMessage());
		}
		return true;

	}

	private boolean checkAge(int age) throws UnderAgeException {
		if (age < 18)
			throw new UnderAgeException("Age is less than 18");

		return true;
	}

	private boolean checkLocality(String locality) throws LocalityNotFoundException {
		for (String s : localities) {
			if (s.equals(locality))
				return true;
		}
		throw new LocalityNotFoundException("Locality is not present");

	}

	private boolean checkVoterId(int vid) throws NoVoterIdException {
		if (vid < 1000 || vid > 9999)
			throw new NoVoterIdException("no voter id is found");

		return true;

	}

}
