package com.voterapp.exception;

@SuppressWarnings("serial")
public class UnderAgeException extends InvalidVoterException {

	public UnderAgeException() {
		super();
	}

	public UnderAgeException(String arg0) {
		super(arg0);
	}

}
