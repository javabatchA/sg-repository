package com.voterapp.exception;

@SuppressWarnings("serial")
public class LocalityNotFoundException extends InvalidVoterException {

	public LocalityNotFoundException() {
		super();
	}

	public LocalityNotFoundException(String arg0) {
		super(arg0);
	}

}
