package com.voterapp.exception;

@SuppressWarnings("serial")
public class InvalidVoterException extends Exception {

	public InvalidVoterException() {
		super();
	}

	public InvalidVoterException(String arg0) {
		super(arg0);
	}

}
