package com.voterapp.main;

import java.util.Scanner;

import com.voterapp.exception.InvalidVoterException;
import com.voterapp.service.ElectionBoothImpl;

public class Main {

	public static void main(String[] args) {
		ElectionBoothImpl electionBoothImpl = new ElectionBoothImpl();

		Scanner myObj = new Scanner(System.in);
		System.out.println("Enter age");
		int age = myObj.nextInt();
		System.out.println("Enter voter id");
		int voterId = myObj.nextInt();
		System.out.println("Enter locality");
		String locality = myObj.next();

		try {
			electionBoothImpl.checkEligibility(age, locality, voterId);
		} catch (InvalidVoterException e) {
			System.out.println(e.getMessage());
		}
	}

}
